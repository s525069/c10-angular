"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var angular2_1 = require('angular2/angular2');
var router_1 = require('angular2/router');
var people_service_1 = require('./people.service');
var PersonDetail = (function () {
    function PersonDetail(_peopleService, _routeParams, _router) {
        var _this = this;
        this._peopleService = _peopleService;
        this._routeParams = _routeParams;
        this._router = _router;
        var id = +this._routeParams.get('id');
        _peopleService.getPerson(id)
            .subscribe(function (res) { return _this.person = res; });
    }
    PersonDetail = __decorate([
        angular2_1.Component({
            selector: 'person-detail',
            templateUrl: './app/people/person.html',
            directives: [angular2_1.CORE_DIRECTIVES, angular2_1.FORM_DIRECTIVES],
            inputs: ['person']
        }), 
        __metadata('design:paramtypes', [people_service_1.PeopleService, (typeof (_a = typeof router_1.RouteParams !== 'undefined' && router_1.RouteParams) === 'function' && _a) || Object, (typeof (_b = typeof router_1.Router !== 'undefined' && router_1.Router) === 'function' && _b) || Object])
    ], PersonDetail);
    return PersonDetail;
    var _a, _b;
}());
exports.PersonDetail = PersonDetail;
